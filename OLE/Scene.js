/**
 * Scene created by bgc.
 * Contact: bgustavocosta@gmail.com
 * Date:  2/6/12
 * Time: 2:31 PM
 */

/**
 * [Scene description]
 * @param {Array} options [description]
 */
function Scene(options) {

/*
************************************************************************
PRIVATE VARIABLES
***********************************************************************
*/

/**
* [default_args description]
* @type {Array}
*/
	var default_args;

/**
* [sceneString description]
* @type {String}
*/
	var sceneString;

	default_args = {
		'div'   :"#map",
		'width' :450,
		'height':450,
		'stat'  :false,
		'log'   :false
	};

/*
************************************************************************
PRIVATE METHODS
***********************************************************************
*/
/**
* [init description]
* @return {void}
*/
	function init(){
		if(!options){
			options = {
				'div'   : "#map",
				'width' : 450,
				'height': 450,
				'stat'  : false,
				'log'   : false
			};
		}
		else{
			for(var index in default_args) {
				if(typeof options[index] == undefined) options[index] = default_args[index];
			}
		}

		sceneString = '<x3d xmlns="http://www.x3dom.org/x3dom"' +
			'id="element"' +
			' showStat="' + options['stat'] +
			'" showLog="' + options['log'] +
			'" width="' + options['width'] + 'px"' +
			'height="' + options['height'] + 'px">' +
			'<worldinfo info="Bruno" title="mapa"></worldinfo>' +
			'<scene DEF="scene">' +
			' <viewPoint id="vp_global" description="GlobalViewPoint" position="196528.039 2803.333 -10999.156"  orientation="-1 0 0 1.57075" lookAt="196528.039 0 -10999.156"></viewPoint>' + //<!-- lookAt="196528.039 0 -10999.156" -->
			'<!-- <navigationinfo id="navInfo" type=\'"Examine" "Any"\'></navigationinfo> -->' +
			'<transform translation="0 0 0" id="entrada">' +
			'<!-- <group><transform translation="3 0 -2"><shape><appearance>	<material diffuseColor=\'0.7 0.4 0.0\' ></material></appearance><cone DEF=\'cone5\'></cone></shape></transform></transform></group> -->' +
			'</transform>' +
			'</scene>' +
			'</x3d>';
		$(options['div']).html(sceneString);
	}

/*
************************************************************************
PRIVILEGED VARIABLES
************************************************************************
*/
/**
* [layers description]
* @type {Array}
*/
	this.layers = [];
/**
 * [camera description]
 * @type {Camera}
 */
	this.camera = new Camera();
	this.camera.setParent(this);
/*
************************************************************************
PRIVILEGED METHODS
************************************************************************
*/
/**
* [addLayer description]
* @param {String} whichLayer [description]
*/
	this.addLayer = function (whichLayer) {

		//TODO: o __proto__ está deprecated
		if (!whichLayer || whichLayer.__proto__.constructor.name !== "Layer") {
//			console.log(whichLayer + "does not exist!");
		}
		else {
//			console.log("adicionado layer");
			this.layers.push(whichLayer);
		}
	};

/**
* [draw description]
* @return {void}
*/
	this.draw = function() {

	};
/**
 * [updateCamera description]
 * @param  {Array} pos [description]
 * @param  {Array} rot [description]
 */
	this.updateCamera = function(pos, rot){

		if( !isNaN(pos.x) || !isNaN(pos.y) || !isNaN(pos.z) ){
			this.camera.setPosition(pos);
		}

		if( !isNaN(rot[0].x) || !isNan(rot[0].y)   || !isNaN(rot[0].z)  || !isNaN(rot[1]) ){
			this.camera.setRotation(rot);
		}

		for( var i = 0; i < this.layers.length; i++){
			if(this.layers[i].visible){
				this.layers[i].recalculateGrid(this.camera.getPosition(), this.camera.getRotation());
			}
		}
	};
	init();
}