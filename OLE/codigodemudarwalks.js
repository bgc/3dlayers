/* =====================================================
This method is called once the system initialized and is ready to render the first time.
It is therefore possible to execute custom action by overriding this method in your code.
===================================================== */
//x3dom.runtime.ready = function()
//{
//var configure = document.getElementById("the_x3dom_element");

/* Deafault view */
//configure.runtime.showAll();

/* ====================================================
Arguments: show (boolean) – true/false to show or hide the debug window
Returns: The current visibility status of the debug window (true/false)
Displays or hides the debug window. If the paramiter is omitted, the current visibility satatus is returned.
==================================================== */
// configure.runtime.debug(false);

/* ====================================================
Arguments: mode (boolean) – true/false to enable or disable the stats info
Returns:The current visibility of the stats info (true/false)
Get or set the visibility of the statistics information.
If parameter is omitted, this method returns the visibility status as boolean.
==================================================== */
//configure.runtime.statistics(false)
//};

//function setNavigationType(typename)
//{
//var configure = document.getElementById("the_x3dom_element");

/* ====================================================
Arguments: typename (string) – walk/examine/lookAround/lookAt/resetView/uprightView/showAll/nextView/prevView
to change mode or viewpoint.
walk() : Switches to walk mode.
examine() : Switches to examine mode.
lookAround() : Switches to lookAround mode.
lookAt() : Switches to lookAt mode.
resetView() : Navigates to the initial viewpoint.
uprightView() : Navigates to upright view.
showAll() : Zooms so that all objects are visible.
nextView() : Navigates to the next viewpoint.
prevView() : Navigates to the previous viewpoint.
Returns: The current mode or viewpoint. (walk/examine/lookAround/lookAt/resetView/uprightView/showAll/nextView/prevView)
Set mode or viewpoint of X3DOM element.
==================================================== */

/*
switch (typename)
{
case 'walk': configure.runtime.walk();
break;
case 'examine': configure.runtime.examine();
break;
case 'lookAround': configure.runtime.lookAround();
break;
case 'lookAt': configure.runtime.lookAt();
break;
case 'resetView': configure.runtime.resetView();
break;
case 'uprightView': configure.runtime.uprightView();
break;
case 'showAll': configure.runtime.showAll();
break;
case 'nextView': configure.runtime.nextView();
break;
case 'prevView': configure.runtime.prevView();
break;
default: configure.runtime.showAll();
}*/