function drawGUI(argument){
	drawNAVMODE("NavMode");
	drawINTERACTIONMODE("InterMode");
}

function drawNAVMODE(argument){

	var controllerDiv	= document.getElementById("ctrl");
	var NavDiv		= document.createElement("div");

	NavDiv.setAttribute("id", argument);
	NavDiv.innerHTML = "<h1>Modo Navegacao</h1>";

	var aButton 		= createButton("onclick", 'switchNavMode("Any");', "Any");
	NavDiv.appendChild(aButton);

	var aButton 		= createButton("onclick", 'switchNavMode("Game");', "Game");
	NavDiv.appendChild(aButton);

	var aButton 		= createButton("onclick", 'switchNavMode("Walk");', "Walk");
	NavDiv.appendChild(aButton);

	var aButton 		= createButton("onclick", 'switchNavMode("Fly");', "Fly");
	NavDiv.appendChild(aButton);

	var aButton 		= createButton("onclick", 'switchNavMode("Examine");', "Examine");
	NavDiv.appendChild(aButton);

	var aButton 		= createButton("onclick", 'switchNavMode("LookAt");', "LookAt");
	NavDiv.appendChild(aButton);

	controllerDiv.appendChild(NavDiv);
}

function drawINTERACTIONMODE(argument){
	var controllerDiv		= document.getElementById("ctrl");
	var InteractDiv			= document.createElement("div");

	InteractDiv.setAttribute("id", argument);
	InteractDiv.innerHTML 	= "<h1>Modo Interaccao</h1>";

	var aButton 			= createButton("onclick", 'switchInteractMode("distancia");', "Distancia");
	InteractDiv.appendChild(aButton);

	var aButton				= createButton("onclick", 'switchInteractMode("ponto");', "Ponto");
	InteractDiv.appendChild(aButton);

	var aButton				= createButton("onclick", 'switchInteractMode("feature");', "Feature");
	InteractDiv.appendChild(aButton);

	var aButton				= createButton("onclick", 'switchInteractMode("none");', "Nenhum");
	InteractDiv.appendChild(aButton);

	controllerDiv.appendChild(InteractDiv);
}

function createButton(functionName, parameterString, NameString){

	var botao	= document.createElement("button");
	botao.setAttribute(functionName, parameterString);
	botao.innerHTML = NameString;
	return botao;

}