/**
 * Grid created by bgc.
 * Contact: bgustavocosta@gmail.com
 * Date:  2/6/12
 * Time: 2:31 PM
 */
/**
 * [Grid description]
 */
function Grid(){
/*
 ************************************************************************
 PRIVATE VARIABLES
 ***********************************************************************
 */
/**
* [tileSizes description]
* @type {Array}
*/
	var tileSizes = [];
/**
* [tileCorner description]
* @type {Array}
*/
/**
 * [maxBoxWidth description]
 * @type {Number}
 */
	var maxBoxWidth;
/**
 * [maxBoxHeight description]
 * @type {Number}
 */
	var maxBoxHeight;
/**
 * [tileCorner description]
 * @type {Array}
 */
	var tileCorner = [];
/*
************************************************************************
PRIVATE METHODS
***********************************************************************
*/


/*
************************************************************************
PRIVILEGED VARIABLES
************************************************************************
*/

/*
************************************************************************
PRIVILEGED METHODS
************************************************************************
*/
/**
* [setTileSizes description]
* @param {Array} tileSizesArray [description]
*/
	this.setTileSizes = function(tileSizesArray){
		tileSizes = tileSizesArray;
	};

/**
* [setTileCorner description]
* @param {Array} tileCornersArray [description]
*/
	this.setTileCorner = function(tileCornersArray){
		tileCorner = tileCornersArray;
	};

/**
 * [setBoxSizes description]
 * @param {Array} boundingBoxArray [description]
 */
	this.setBoxSizes = function(boundingBoxArray){
		maxBoxWidth = boundingBoxArray[2] - boundingBoxArray[0];
		maxBoxHeight = boundingBoxArray[3] - boundingBoxArray[1];
	};

/**
* [getCorner description]
* @return {Array}
*/
	this.getCorner = function(){
		return tileCorner;
	};

/**
* [getSizes description]
* @return {Array}
*/
	this.getSizes = function(){
		return tileSizes;
	};

/**
 * [getColumn description]
 * @return {Number}
 */
	this.getColumn = function() {
	return this.currentColumn;
	};

/**
 * [getRow description]
 * @return {Number}
 */
	this.getRow = function() {
	return this.currentRow;
	};

/**
 * [setRow description]
 * @param {Number} value [description]
 */
	this.setRow = function(value){
//		console.log("update da ROW");
		this.currentRow = value;
	};

/**
 * [setColumn description]
 * @param {Number} value [description]
 */
	this.setColumn = function(value){
//		console.log("update da COL");
		this.currentColumn = value;
	};

/**
 * [calculateColumn description]
 * @param  {Array} pos [description]
 * @param  {Array} rot [description]
 * @return {Number}
 */
	this.calculateRow = function(pos, rot){
//X
		var col = Math.floor( (parseFloat(pos[0]) - parseFloat(tileCorner[0])) / parseFloat(tileSizes[0]));
		if(debug === true){console.log("A coluna da Grid é : " + col);}
		return col;
	};

/**
 * [calculateRow description]
 * @param  {Array} pos [description]
 * @param  {Array} rot [description]
 * @return {Number}
 */
	this.calculateColumn = function(pos, rot){
//Z
		var row = Math.floor( (pos[2] - parseFloat(tileCorner[1])) / parseFloat(tileSizes[0]));
	if(debug === true){console.log("A row da Grid é : " + row);}
		return row;
	};

/**
 * [currentRow description]
 * @type {Number}
 */
	this.currentRow = -1;

/**
 * [currentColumn description]
 * @type {Number}
 */
	this.currentColumn = -1;
}