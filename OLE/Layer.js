/**
 * Layer created by bgc.
 * Contact: bgustavocosta@gmail.com
 * Date:  2/6/12
 * Time: 2:31 PM
 */
/**
 * [Layer description]
 * @param {String} urlEntry       [description]
 * @param {String} nameLayerEntry [description]
 */
function Layer(urlEntry, nameLayerEntry) {

	/*
	 ************************************************************************
	 PRIVATE VARIABLES
	 ***********************************************************************
	 */
	/**
	 * [url description]
	 * @type {String}
	 */
	var url = urlEntry;
	/**
	 * [xhr description]
	 * @type {XMLHttpRequest}
	 */
	var xhr = null;
	/**
	 * [isDesiredNode description]
	 * @type {Boolean}
	 */
	var isDesiredNode = false;
	/**
	 * [styles description]
	 * @type {Array}
	 */
	var styles = [];
	/**
	 * [that description]
	 * @type {self}
	 */
	var that = this;
	/**
	 * [tiles description]
	 * @type {Array}
	 */
	var tiles = new Array();
	for (var i = 0; i < 12; i++) {
		var arraydeCols = [];
		for (var j = 0; j < 14; j++) {
			var tempTile = new Tile();
			arraydeCols.push(tempTile);
		}
		tiles.push(arraydeCols);
	}
	/*
	 ************************************************************************
	 PRIVILEGED VARIABLES
	 ***********************************************************************
	 */
	/**
	 * [tiled description]
	 * @type {Boolean}
	 */

	/* ROW 11
	 COL 13
	 */
	this.tiled = false;

	/**
	 * [visible description]
	 * @type {Boolean}
	 */
	this.visible = false;
	/**
	 * [boundingBox description]
	 * @type {BBox}
	 */
	this.boundingBox = new BBox();
	/**
	 * [nameLayer description]
	 * @type {String}
	 */
	this.nameLayer = nameLayerEntry;

	/*
	 ************************************************************************
	 PRIVATE METHODS
	 ************************************************************************
	 */
	/**
	 * Initialization
	 * @return {void}
	 */
	function init() {
		console.log(that.nameLayer);
		if (!url || !that.nameLayer) {
			console.log("error in Layer parameters.");
		}
		else {
			var urlCompleted = url +
							   "?SERVICE=W3DS&REQUEST=GetCapabilities&ACCEPTVERSIONS=0.4.0";
			var contents = null;
			//TODO: TEMPORARIO: PARA A DEMO
			urlCompleted = "capabilities.xml";
//			if(debug === true){console.log("Getting capabilities from "+ urlCompleted);}

			xhr = $.get('proxy.php', {bounds:urlCompleted}, function (data) {
			})

				.success(function () {
//					if(debug === true){console.log("Success getting layer\n*Now parsing capabilities");}
					var parser = new DOMParser();
					var xmlDoc = parser.parseFromString(xhr.responseText,"text/xml");

					contents = xmlDoc.getElementsByTagName("Contents")[0].childNodes;
				})

				.error(function () {
					if (debug === true) {
						console.log("!!!!Error getting layer.");
					}
				})

				.complete(function () {

					isDesiredNode = false;
//					if(debug === true){console.log("*Searching for layer with name "+that.nameLayer);}
					for (var i = 0; i < contents.length; i++) {
						if (contents.item(i).nodeType == 1 && contents.item(i).nodeName == "Layer" && isDesiredNode === false) {
							var currentNode = contents.item(i).childNodes;
							obtainLayer(currentNode);
						}
					}
//					if(debug === true){console.log("*Completed Layer Request");}
				});
		}

	}

	/**
	 * Function to obtain a layer by name
	 * @param  {Node} nodeParsingLayer [description]
	 * @return {void}
	 */
	function obtainLayer(nodeParsingLayer) {
		var j = 0;
		while (j < nodeParsingLayer.length && isDesiredNode === false) {
			if (nodeParsingLayer.item(j).nodeName == "ows:Identifier") {
				if (nodeParsingLayer.item(j).childNodes[0].nodeValue === that.nameLayer) {
//					if(debug === true){console.log("--->Found desired layer with name "+ nodeParsingLayer.item(j).childNodes[0].nodeValue);}

					that.tiled = isTiled(nodeParsingLayer);
					if (that.tiled === true) {
						that.grid = new Grid();
						that.grid.setTileSizes(obtainTiles(nodeParsingLayer));
						that.grid.setTileCorner(obtainCorner(nodeParsingLayer));
					}

					if (hasStyles(nodeParsingLayer)) {
//						if(debug === true){console.log("************Styles :");}
						styles = obtainStyles(nodeParsingLayer);
//						if(debug === true){console.log("----------->" + styles);}
					}

					that.boundingBox.setBoundsFromArray(obtainBBox(nodeParsingLayer));

					if (that.tiled === true) {
						that.grid.setBoxSizes(that.boundingBox.getBounds());
					}

					isDesiredNode = true;
				}
			}
			j++;
		}

	}

	/**
	 * Function used to check if the current layer has any tiles or is a single tile
	 * @param  {Node}  nodeParsingTiled [description]
	 * @return {Boolean}
	 */
	function isTiled(nodeParsingTiled) {
		var returnValue;

		for (var k = 0; k < nodeParsingTiled.length; k++) {
			if (nodeParsingTiled.item(k).nodeName == "Tiled") {
//				if(debug === true){console.log("--->found a Tiled Node with value " + nodeParsingTiled.item(k).childNodes[0].nodeValue);}
				returnValue =
				Boolean(nodeParsingTiled.item(k).childNodes[0].nodeValue.toLowerCase());
			}
		}
		return returnValue;
	}

	/**
	 * Function used to obtain the current layer's tileSizes
	 * @param  {Node} TilesNode [description]
	 * @return {Array}
	 */
	function obtainTiles(TilesNode) {

		var tileValues = [];

		for (var m = 0; m < TilesNode.length; m++) {
			if (TilesNode.item(m).nodeName == "TileSet") {
//				if(debug === true){console.log("------->found a TileSet Node");}
				for (var n = 0; n < TilesNode.item(m).childNodes.length; n++) {
					if (TilesNode.item(m).childNodes[n].nodeName == "TileSizes") {
						var tileValue = TilesNode.item(m).childNodes[n].childNodes[0].nodeValue.split(" ");
						for (var o = 0; o < tileValue.length; o++) {
							tileValues[o] = parseFloat(tileValue[o]);
						}
					}
				}
			}
		}
		return tileValues;
	}

	/**
	 *Function used to obtain the current layer's bounding box lower corner coordinates
	 * @param  {Node} TilesNode [description]
	 * @return {Array}
	 */
	function obtainCorner(TilesNode) {

		var tileCorners = [];

		for (var m = 0; m < TilesNode.length; m++) {

			if (TilesNode.item(m).nodeName == "TileSet") {

//				if(debug === true){console.log("------->found a TileSetCorner Node");}

				for (var n = 0; n < TilesNode.item(m).childNodes.length; n++) {

					if (TilesNode.item(m).childNodes[n].nodeName == "LowerCorner") {
						var tileCorner = TilesNode.item(m).childNodes[n].childNodes[0].nodeValue.split(" ");

						for (var o = 0; o < tileCorner.length; o++) {
							tileCorners[o] = parseFloat(tileCorner[o]);
						}
					}
				}
			}
		}
		return tileCorners;
	}

	/**
	 * Function used to obtain the bounaries of the current layer's bounding Box
	 * @param  {Node} nodeParsingBounds [description]
	 * @return {Array}
	 */
	function obtainBBox(nodeParsingBounds) {
		var result = [];
		var valueFoundLower = null;
		var valueFoundUpper = null;

		for (var i = 0; i < nodeParsingBounds.length; i++) {

			if (nodeParsingBounds.item(i).nodeName == "ows:BoundingBox" && nodeParsingBounds.item(i).getAttribute("crs") == "EPSG:27492") {

//				if(debug === true){console.log("--->found a BBox Node");}

				for (var j = 0; j < nodeParsingBounds.item(i).childNodes.length; j++) {

					if (nodeParsingBounds.item(i).childNodes[j].nodeName == "ows:LowerCorner") {

//						if(debug === true){console.log("------->Found LowerCorner");}

						valueFoundLower = nodeParsingBounds.item(i).childNodes[j].childNodes[0].nodeValue.split(" ");
					}
					else if (nodeParsingBounds.item(i).childNodes[j].nodeName == "ows:UpperCorner") {

//						if(debug === true){console.log("------->Found UpperCorner");}

						valueFoundUpper =
						nodeParsingBounds.item(i).childNodes[j].childNodes[0].nodeValue.split(" ");
					}
				}

			}
		}
		if (valueFoundUpper !== null && valueFoundLower !== null) {
			result[0] = valueFoundLower[0];
			result[1] = valueFoundLower[1];
			result[2] = valueFoundUpper[0];
			result[3] = valueFoundUpper[1];
		}
		return result;
	}

	/**
	 * Function used to check if current layer has any styles attached to it
	 * @param  {Node}  nodeParsingStyles [description]
	 * @return {Boolean}
	 */
	function hasStyles(nodeParsingStyles) {
		var valueStyle = false;
		for (var k = 0; k < nodeParsingStyles.length; k++) {
			if (nodeParsingStyles.item(k).nodeName == "Style") {
//				if(debug === true){console.log("--->found a Style Node");}
				valueStyle = true;
			}
		}
		return valueStyle;
	}

	/**
	 * Function used to obtain the current layer style descriptions
	 * @param  {Node} nodeParsingStyles [description]
	 * @return {Array}
	 */
	function obtainStyles(nodeParsingStyles) {
		var valueStyles = [];
		for (var m = 0; m < nodeParsingStyles.length; m++) {
			if (nodeParsingStyles.item(m).nodeName == "Style") {
//				if(debug === true){console.log("------->Processing a Style Node");}
				for (var n = 0; n < nodeParsingStyles.item(m).childNodes.length; n++) {
					if (nodeParsingStyles.item(m).childNodes[n].nodeName == "ows:Identifier") {
						var styleValue = nodeParsingStyles.item(m).childNodes[n].childNodes[0].nodeValue;
						valueStyles.push(styleValue);
					}
				}
			}
		}
		return valueStyles;
	}

	/**
	 * [getTile description]
	 * @param  {Number} column     [description]
	 * @param  {Number} row        [description]
	 * @param  {Number} level      [description]
	 * @param  {String} layer      [description]
	 * @param  {String} crs        [description]
	 * @param  {String} layerStyle [description]
	 */
	function getTile(column, row, level, layer, crs, layerStyle) {
//		console.log("*****Entrei no getTile")
		var obtainedTile = null;

		if (!column || !row || !level || !layer) console.log("error in Tile parameters.");
		else {
			if (!crs) {
				crs = "EPSG:27492";
			}
			/*if(layerStyle === null){ layerStyle = styles[0];}*/
			//var urlCompleted = url + "?SERVICE=W3DS&REQUEST=GetTile&ACCEPTVERSIONS=0.4.0&FORMAT=model/x3d&CRS="+crs+ "&LAYER="+layer+"&TILELEVEL="+level + "&TILEROW="+row+"&TILECOL="+column;
			//TODO:MARTELADA DO URL
			var urlCompleted;
			//"http://localhost/3dlayers/guim_6_12.x3d";
			urlCompleted = url + "?version=0.4&service=w3ds&request=GetTile&CRS=" + crs + "&FORMAT=model/x3d+xml&LAYER=" + layer + "&TILELEVEL=1&TILEROW=" + row + "&TILECOL=" + column;
			//http://129.206.229.148/EUROPE/W3DS?SERVICE=W3DS&REQUEST=GetTile&VERSION=0.4.0&CRS=EPSG:900913&FORMAT=model/vrml&LAYER=DEM&TILELEVEL=7&TILEROW=338&TILECOL=268;
			var tileContents = null;
			if (debug === true)console.log("Getting Tile X3D from " + urlCompleted);
			console.log(tiles[row]);

			tiles[row][column] = new Tile();
			console.log(tiles[row][column]);
			tiles[row][column].isLoading = true;

			var tileXHR = $.get('proxy.php', {bounds:urlCompleted}, function (data) { })


				.success(function () {

							//if(debug === true){console.log("Success getting Tile\n*Now parsing info");}
							var parser = new DOMParser();

							var x3dDoc = parser.parseFromString(tileXHR.responseText,"text/xml");
							//TODO:ISTO NAO FUNCIONA NO FIREFOX
							tileContents = x3dDoc.getElementById(that.nameLayer).cloneNode(true);
							console.log(tileContents.childNodes);

							var placeOfEntry;

							if (!document.getElementById(that.nameLayer)) {
								placeOfEntry = document.createElement("Group");
								placeOfEntry.setAttribute("id", that.nameLayer);
								placeOfEntry.setAttribute("onclick", "getFeature(event.hitPnt);") //event.hitObject,
								document.getElementById("entrada").appendChild(placeOfEntry);
							}

							var transformScale = document.createElement("Transform");
							transformScale.setAttribute("scale", "1 1 1");
							//TODO: AQUI TESTE PARA ADICIONAR AOS SHAPES O GETFEATURE
							/*for (var m = 0; m < tileContents.childNodes.length; m++) {
								for (var n = 0; n < tileContents.item(m).childNodes.length; n++) {
									if (tileContents.item(m).childNodes[n].nodeName == "shape") {
										console.log("ACHEI UM SHAPE");
										var shapeValue = tileContents.item(m).childNodes[n];
										shapeValue.setAttribute("onclick", "getFeature(event.hitPnt;);");
									}
								}
							}*/

							var theShapes = tileContents.getElementsByTagName("shape");
							console.log("EI: " + theShapes[0].length);



							for (var k = 1; k < tileContents.childNodes.length; k += 2) {
								transformScale.appendChild(tileContents.childNodes[k].cloneNode(true));
							}

							//transformScale.appendChild(tileContents.childNodes[]);

							placeOfEntry = document.getElementById(that.nameLayer);
							placeOfEntry.appendChild(transformScale);
							//TODO:MARTELADO DA SCENE, NAO DEVIA ESTAR AQUI
							that.getScene(column, row);
						})


				.error(function () {
//					if(debug === true){console.log("!!!!Error getting Tile.");}
					   })


				.complete(function () {
					tiles[row][column].isLoading = false;
					tiles[row][column].isLoaded = true;
				}
			);
			var contents = null;
		}
	}

	/**
	 * [needsGridRecalculation description]
	 * @param  {Array} posArray [description]
	 * @param  {Array} rotArray [description]
	 * @return {Boolean}
	 */
	function needsGridRecalculation(posArray, rotArray) {
		var currentRow = that.grid.getRow();
		var currentColumn = that.grid.getColumn();
		console.log(currentRow);
		var newRow = that.grid.calculateRow(posArray, rotArray);
		var newColumn = that.grid.calculateColumn(posArray, rotArray);
		console.log(newRow);
		var needsUpdate = false;
		if (newRow <= 0 || newColumn <= 0) {
			console.log("********A layer " + that.nameLayer + " nao tem uma grelha existente para coordenadas actuais");
		}
		else {
			console.log("********A layer " + that.nameLayer + " tem uma grelha existente para coordenadas actuais");
			if (newRow != currentRow && newRow < 12) {
				console.log("A GRELHA PRECISA DE UPDATE PARA O ROW");
				that.grid.setRow(newRow);
				needsUpdate = true;
			}
			if (newColumn != currentColumn && newColumn < 14) {
				console.log("A GRELHA PRECISA DE UPDATE PARA A COLUMN");
				that.grid.setColumn(newColumn);
				needsUpdate = true;
			}
		}
		return needsUpdate;
	}

	/*
	 ************************************************************************
	 PRIVILEGED METHODS
	 ************************************************************************
	 */
	/**
	 * [recalculateGrid description]
	 * @param  {Array} posArray [description]
	 * @param  {Array} rotArray [description]
	 */
	this.recalculateGrid = function (posArray, rotArray) {

		var doesNeedTiles;


		doesNeedTiles = needsGridRecalculation(posArray, rotArray);
		var aRow = that.grid.getRow();
		var aCol = that.grid.getColumn();

//		console.log("*******esta carregada? " + tiles[aRow][aCol].isLoaded);
//		console.log("*******esta a carregar?" + tiles[aRow][aCol].isLoading);

		if (doesNeedTiles === true &&
			tiles[that.grid.getRow()][that.grid.getColumn()].isLoaded ===
			false &&
			tiles[that.grid.getRow()][that.grid.getColumn()].isLoading ===
			false) {
			console.log("Vou Executar o getTile");
			getTile(that.grid.getColumn(), that.grid.getRow(), 1, that.nameLayer);
		}
	};
	this.commandTiles = function (row, column) {
		console.log(row + " " + column + " " + that.nameLayer);
		getTile(row, column, 1, that.nameLayer);
	};
	this.putTiles = function () {
		console.log(tiles);
	};
	this.getScene = function (row, col) {
		var urlFinal = url + "?version=0.4&service=w3ds&request=GetScene&crs=EPSG:27492&format=model/x3d+xml&layers=caixas_postes_t,caixas_predios_t,caixas_c_poste_t,caixas_tracados_aereo_parede_t,caixas_armarios_t,caixas_tracados_aereo_t,caixas_cabos_conduta_t,caixas_camaras_t&boundingbox=";
		var bboxFinal = [];

		bboxFinal.push(that.grid.getCorner()[0] + (col * that.grid.getSizes()[0]));

		bboxFinal.push(that.grid.getCorner()[1] + (row * that.grid.getSizes()[0]));

		bboxFinal.push(that.grid.getCorner()[0] + (col * that.grid.getSizes()[0]) + that.grid.getSizes()[0]);

		bboxFinal.push(that.grid.getCorner()[1] + (row * that.grid.getSizes()[0]) + that.grid.getSizes()[0]);

		urlFinal = urlFinal + bboxFinal[1] + "," + bboxFinal[0] + "," + bboxFinal[3] + "," + bboxFinal[2];
		console.log(urlFinal);
		var tileXHR = $.get('proxy.php', {bounds:urlFinal }, function (data) {
		})
			.success(function () {
				var parser = new DOMParser();

				var x3dDoc = parser.parseFromString(tileXHR.responseText,"text/xml");
				//TODO:ISTO NAO FUNCIONA NO FIREFOX
				tileContents = x3dDoc.getElementsByTagName("Scene")[0].cloneNode(true);
				console.log(tileContents.childNodes);

				var placeOfEntry;

				if (!document.getElementById(that.nameLayer)) {
					placeOfEntry = document.createElement("Group");
					placeOfEntry.setAttribute("id", that.nameLayer);
					document.getElementById("entrada").appendChild(placeOfEntry);
				}

				var transformScale = document.createElement("Transform");
				transformScale.setAttribute("scale", "1 1 1");

				for (var k = 1; k < tileContents.childNodes.length; k += 2) {
					transformScale.appendChild(tileContents.childNodes[k].cloneNode(true));
				}

				//transformScale.appendChild(tileContents.childNodes[]);

				placeOfEntry = document.getElementById(that.nameLayer);
				placeOfEntry.appendChild(transformScale);


			})
			.error(function () {
				})
			.complete(function () {
					});

	}
	init();
}


//   http://192.168.1.255:9090/geoserver/w3ds?version=0.4&service=w3ds&request=GetTile&CRS=EPSG:27492&FORMAT=model/x3d+xml&LAYER=dem_barcelos_texturas&TILELEVEL=1&TILEROW=2&TILECOL=4