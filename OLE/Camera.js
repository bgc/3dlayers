/**
 * Camera created by bgc.
 * Contact: bgustavocosta@gmail.com
 * Date: 2/7/12
 * Time: 6:31 PM
 */
/**
 * [Camera description]
 */
function Camera() {

/*
************************************************************************
PRIVATE VARIABLES
***********************************************************************
*/
	/**
	 * [position description]
	 * @type {Array}
	 */
	var position = [];
/**
 * [rotation description]
 * @type {Array}
 */
	var rotation = [];
	
	
	var parent;

/**
* [centerPoint description]
* @type {Array}
*/
	var centerPoint = [];
/**
* [boundingBox description]
* @type {BBox}
*/
	var boundingBox = new BBox();

/*
************************************************************************
PRIVATE METHODS
***********************************************************************
*/

/*
************************************************************************
PRIVILEGED VARIABLES
************************************************************************
*/

/*
************************************************************************
PRIVILEGED METHODS
************************************************************************
*/
/**
 * [setPosition description]
 * @param {[type]} posArray [description]
 */
	this.setPosition = function(posArray){
		position[0] = posArray.x;
		position[1] = posArray.y;
		position[2] = posArray.z;
	};
/**
 * [setRotation description]
 * @param {[type]} rotArray [description]
 */
	this.setRotation = function(rotArray){
		rotation[0] = rotArray[0].x;
		rotation[1] = rotArray[0].y;
		rotation[2] = rotArray[0].z;
		rotation[3] = rotArray[1];
	};
/**
 * [setParent description]
 * @param {[type]} parentObject [description]
 */
	this.setParent = function(parentObject){
		parent = parentObject;
	};
/**
 * [getParent description]
 * @return {[type]}
 */
	this.getParent = function(){
		return parent;
};
/**
 * [getPosition description]
 * @return {[type]}
 */
	this.getPosition = function(){
		return position;
	};
/**
 * [getRotation description]
 * @return {[type]}
 */
	this.getRotation = function(){
		return rotation;
	};
}