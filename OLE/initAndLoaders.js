function initMap(options){
	var default_args = {
		'div'		:	"#map",
		'width'		:	450,
		'height'	:	450,
		'stat'		:	false,
		'log'		:	false,
		'avatar'	:	true
	};
	if(!options){
		options = {
				'div'		:	"#map",
				'width'		:	450,
				'height'	:	450,
				'stat'		:	false,
				'log'		:	false,
				'avatar'	:	true
		};
	}
	else{
		for(var index in default_args) {
			if(typeof options[index] === undefined) options[index] = default_args[index];
		}
	}
	var avatarString;
	if(options['avatar'] === true){
		avatarString = '<viewPoint id="vp_avatar" description="AvatarViewPoint" retainUserOffsets="TRUE" position="0 0 10"></viewPoint>';
		Mapa['avatar'] = true;
	} else {
		avatarString = " ";
		Mapa['avatar'] = false;
	}
	var sceneString = '<x3d xmlns="http://www.x3dom.org/x3dom"'+
						'id="element"'+
						' showStat="'+options['stat']+
						'" showLog="'+options['log']+
						'" width="'+options['width']+'px"'+
						'height="'+options['height']+'px">'+
						'<worldinfo info="Bruno" title="mapa"></worldinfo>'+
						'<scene DEF="scene">'+
							'<viewPoint id="vp_global" description="GlobalViewPoint" retainUserOffsets="TRUE" position="0 0 10" set_bind="TRUE"></viewPoint>'+
							avatarString + 
							'<navigationinfo id="navInfo" type=\'"Examine" "Any"\'></navigationinfo>'+
										'<transform translation="0 0 0" id="entrada">'+
										'</transform>'+
						'</scene>'+
					'</x3d>';
	$(options['div']).html(sceneString);
	
}

function loadGeometry(options){
	var default_args = {
		'source'		:	"madeira.x3d",
		'container'		:	"objectos",
		'layerName'		:	"base",
		'properties'	:	false,
		'hasScene'		: true
	};
	if(!options){
		options = {
				'source'		:	"madeira.x3d",
				'container'		:	"objectos",
				'layerName'		:	"base",
				'properties'	:	false,
				'hasScene'		:	false
		};
	}
	else{
		for(var index in default_args) {
			if(typeof options[index] === undefined) options[index] = default_args[index];
		}
	}
	var xhr = $.get('proxy.php', {bounds: options['source']}, function(data){})
	.success(function(){
		var	parser	= new DOMParser();
		var xmlDoc	= parser.parseFromString(xhr.responseText,"text/xml");
		var bicho;
		switch(options['hasScene']){
			case (true):
			//O MADEIRA ESTA EM MAIUSCULAS E O BARCELOS NAO
				bicho = xmlDoc.getElementsByTagName("Scene")[0].childNodes;
			break;
			case (false):
				bicho = xmlDoc.firstChild.childNodes;
			break;
			default:
			bicho = xmlDoc.firstChild.childNodes;
			break;
		}
		var localDeEntrada	= document.getElementById(options['container']);
		var grupoEntrada	= document.createElement("Group");
		var transformEscala	= document.createElement("transform");
		var grupo			= document.getElementById(options['container']);

		transformEscala.setAttribute("scale", "1 1 1" );

		for(var i = 1; i < bicho.length; i+=2){
			//HACK:PORQUE É QUE USO O CLONE NODE???????????????????? SEM CLONE NODE SO FUNCIONAM OS 2 QUANDO SE FAZ AO CONTRÁRIO. NORMAL SO USANDO MESMO O CLONENODE
			transformEscala.appendChild(bicho[i].cloneNode(true));
		}
		grupoEntrada.appendChild(transformEscala);
		localDeEntrada.appendChild(grupoEntrada);
		grupo.lastChild.id = options['layerName'];
		//localDeEntrada.lastChild.id = options['layerName'];
		updateDosTransforms(options['layerName']);
		updateCtrl(options['layerName']);
		if(options['properties'] === true){
			getProperties();
		}
	})
	.error(function(){
		alert("error");
	});
}