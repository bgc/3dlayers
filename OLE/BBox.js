/**
 * BBox created by bgc.
 * Contact: bgustavocosta@gmail.com
 * Date:  2/6/12
 * Time: 2:31 PM
 */

/**
 * [BBox description]
 */
function BBox(){
/*
************************************************************************
PRIVATE VARIABLES
***********************************************************************
*/

/**
* bounds Array for the 4 bounds(LEFT, BOTTOM, RIGHT & TOP) that contain the Layer
* @type {Array}
*/
	var bounds = [];

	bounds[0] = null;
	bounds[1] = null;
	bounds[2] = null;
	bounds[3] = null;

/*
************************************************************************
PRIVILEGED METHODS
************************************************************************
*/

/**
* getBounds returns the bounds Array
* @return {Array}
*/
	this.getBounds = function(){
		return bounds;
	};

/**
* [setBounds fills the bounds Array with the corresponding values using 4 separate input values]
* @param {Number} lowercorner1 [Left limit of Bounding Box]
* @param {Number} lowercorner2 [Bottom limit of Bounding Box]
* @param {Number} uppercorner1 [Right limit of Bounding Box]
* @param {Number} uppercorner2 [Upper limit of Bounding Box]
*/
	this.setBounds = function(lowercorner1, lowercorner2, uppercorner1, uppercorner2){
		//LEFT
		if(lowercorner1 !== null){
			bounds[0] = parseFloat(lowercorner1);
		}
		//BOTTOM
		if(lowercorner2 !== null){
			bounds[1] = parseFloat(lowercorner2);
		}
		//RIGHT
		if(uppercorner1 !== null){
			bounds[2] = parseFloat(uppercorner1);
		}
		//TOP
		if(uppercorner2 !== null){
			bounds[3] = parseFloat(uppercorner2);
		}
	};

/**
* [setBoundsFromArray fills the bounds Array with the corresponding values
* using an Array]
* @param {Array} boundsArray [Array containing the 4 Bounding Box limits]
*/
	this.setBoundsFromArray = function(boundsArray){
		if(boundsArray !== null){
			bounds = boundsArray;
		}
	};
}
